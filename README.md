# Setting up the Asset Management Service

## What you will be building
- CRUD API for managing Assets
- Configuring to fetch the Configuration data
- Register with the Eureka Server 
- Communicate with Organization service over
  - Rest Template with DiscoveryClient
  - Ribbon
  - Feign Client

## Prerequisite
 - Config server should be running on `8888` port
 - Eureka server should be running on `8761` port
 - `OrganizationService` micorservice should be up and registered with Eureka server

## Project Structure
 - controllers
 - services
 - model
 - repository
 - config

### Maven dependencies 
```xml 
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.3.1.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>com.classpath</groupId>
	<artifactId>assets-services-eureka-client</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>assets-services-eureka-client</name>
	<description>Demo project for Spring Boot</description>

	<properties>
		<java.version>1.8</java.version>
		<spring-cloud.version>Hoxton.SR6</spring-cloud.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<version>1.4.200</version>
		</dependency>
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>8.0.20</version>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>1.18.12</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<scope>runtime</scope>
			<optional>true</optional>
		</dependency>
	</dependencies>

```
 ### Item class
 ```java
@Setter
@Getter
@EqualsAndHashCode(of = "itemId")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long itemId;

    private String name;

    private String desc;
}
 ```
### ItemRepository
```java
@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    List<Item> findAll();
}
```

### ItemService interface 
```java
public interface ItemService {

    Item save(Item item);

    List<Item> listAll();

    Item findItemById(long itemId);

    Item updateItem(long itemId, Item item);

    void deleteItemById(long itemId);

}
```

### ItemService Implementation 
```java
@Service
public class ItemServiceImpl implements ItemService {

    private ItemRepository itemRepository;

  
    @Autowired
    private RestTemplate restTemplate;

  
    public ItemServiceImpl(ItemRepository itemRepository){
        this.itemRepository = itemRepository;
    }

    private static IllegalArgumentException invalidItemId() {
        return new IllegalArgumentException("Invalid itemid");
    }

    @Override
    public Item save(Item item) {
        return this.itemRepository.save(item);
    }

    @Override
    public List<Item> listAll() {
        return this.itemRepository.findAll();
    }

    @Override
    public Item findItemById(long itemId) {
       return this.itemRepository
                .findById(itemId)
                .orElseThrow(ItemServiceImpl::invalidItemId);
    }

    @Override
    public Item updateItem(long itemId, Item item) {
        Optional<Item> itemOptional = this.itemRepository.findById(itemId);
        if (itemOptional.isPresent()){
            Item itemFromRepository = itemOptional.get();
            itemFromRepository.setDesc(item.getDesc());
            itemFromRepository.setName(item.getDesc());
            itemFromRepository.setName(item.getName());
            return  this.itemRepository.save(itemFromRepository);
        }
        throw new IllegalArgumentException("Invalid Id");
    }

    @Override
    public void deleteItemById(long itemId) {
        Optional<Item> optionalItem = this.itemRepository.findById(itemId);
        if (optionalItem.isPresent()){
            this.itemRepository.deleteById(itemId);
        }
    }
```

### AssetController
```java

@RestController
@RequestMapping("/api/v1/assets")
public class AssetController {

    private static final Logger logger = LoggerFactory.getLogger(AssetController.class);


    private ItemService assetService;


    @Autowired
    private ApplicationContext applicationContext;


    public AssetController(ItemService assetService){
        this.assetService = assetService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Item saveItem(@RequestBody Item item){
        logger.debug("Came inside the item {}", item);
        return this.assetService.save(item);
    }

    @GetMapping
    public List<Item> fetchAll(){
        logger.debug("Fetching all the items ");
        return this.assetService.listAll();
    }

    @GetMapping(value = "/{itemId}")
    public Item fetchItemById( @PathVariable ("itemId") long itemId){
        return this.assetService.findItemById(itemId);
    }

    @PutMapping(value = "/{itemId}")
    public Item fetchItemById( @PathVariable ("itemId") long itemId, @RequestBody Item item){
        return this.assetService.updateItem(itemId, item);
    }

    @DeleteMapping(value = "/{itemId}")
    public void deleteItemById( @PathVariable ("itemId") long itemId){
        this.assetService.deleteItemById(itemId);
    }

    @GetMapping("/beans")
    public List<String> getBeans(){
        return Arrays.asList(this.applicationContext.getBeanDefinitionNames());
    }

    //@ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleRuntTimeException(Exception exception){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Exception thrown from the asset controller error handler");
    }
}
```

## Fetch the application configuration from Spring Cloud Config Server

- pom.xml (Maven dependencies)
```xml
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-config-client</artifactId>
			<version>2.1.3.RELEASE</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-rsa</artifactId>
		</dependency>
```

- bootstrap.yml `/src/main/resources/`
```yaml
spring:
  application:
    name: assetmgmt
  profiles:
    active: dev
  cloud:
    config:
      uri: http://localhost:8888

eureka:
  client:
    fetch-registry: true
    register-with-eureka: true
    service-url:
      defaultZone: http://localhost:8761/eureka
  instance:
    prefer-ip-address: true

encrypt:
  key: TESTING
```

- application.yml `/src/main/resources`
```yaml
server:
  port: 8111
```

## Testing the REST API's using POSTMAN client

- `HTTP GET` - ```http://localhost:8111/api/v1/assets/```
- `HTTP POST` - ``http://localhost:8111/api/v1/assets/ ``
   ```json
     {
	  "name":"IPhone 12",
	  "desc": "Latest phone from Apple"
     }
   ```
- `HTTP GET` - ``http://localhost:8111/api/v1/assets/2``   
- `HTTP PUT` - ``http://localhost:8111/api/v1/assets/2`` 
  ```json
  {
	"name":"IPhone-13",
	"desc": "Latest phone from Apple"
  }
  ```  
- `HTTP DELETE` - ``http://localhost:8111/api/v1/assets/1``

## Viewing the data in H2 - console
- Open the `http://localhost:8111/h2-console` in Chrome browser
- Run the select query `SElECT * FROM ITEM;`
- Verify the data saved in the DB

### Registering with Eureka as Client 
- Maven dependencies `pom.xml`
```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
```

- `bootstrap.yml`
```yaml
eureka:
  client:
    fetch-registry: true
    register-with-eureka: true
    service-url:
      defaultZone: http://localhost:8761/eureka
  instance:
    prefer-ip-address: true
```
- Add `@EnableDiscoveryClient` annotation

```java
@SpringBootApplication
@EnableDiscoveryClient
public class AssetsServicesEurekaClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssetsServicesEurekaClientApplication.class, args);
	}
}
```

### Verify the Registration with Eureka Server
- Open the `http://localhost:8761/eureka` in the Chrome browser
- You should see the `ASSETMGMT` service registered
- You should also see the `ORGANIZATIONSERVICE` registered in the Eureka server

## Communication between Microservices

### What we will be building
- AssetController will expose an endpoint to fetch the Organization details for a given org id 
- ItemService will call the REST endpoint to Organization service using RestTemplate 

- Required depenecies
  - `Ribbon`
  - `Feign`

- Maven dependency

```xml
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
</dependency>
```
- `AssetController` changes
```java
@GetMapping(value = "/org/{orgId}")
public Organization getOrgById(@PathVariable("orgId") long orgId){
  return this.assetService.fetchOrgByOrgId(orgId);
}
```

- `ItemServiceImpl` classes

### Option 1 - Discovery client

- Configure `RestTemplate` bean
```java
 Configure `RestTemplate` 
```java
@Configuration
public class AppConfiguration {
  @Bean
  public RestTemplate restTemplate(){
      return new RestTemplate();
  }
}
```
 - Configure `DiscoveryClient`
```java
@Autowired
private DiscoveryClient discoveryClient;

 @Override
    //Naive implementation
    public Organization fetchOrgByOrgId(long orgId) {
    List<ServiceInstance> organizationservice = this.discoveryClient.getInstances("ORGANIZATIONSERVICE");
    if (!organizationservice.isEmpty()) {
            ServiceInstance serviceInstance = organizationservice.get(0);
            URI uri = serviceInstance.getUri();
            Organization organization = this.restTemplate.getForEntity(uri+"/api/v1/organization/"+orgId, Organization.class).getBody();
            System.out.println("Organization from the rest response "+ organization);
            return organization;
    }
```
### Option 2 - Using Ribbon

- Configure `RestTemplate` 
```java
@Configuration
public class AppConfiguration {
  @Bean
  @LoadBalanced
  public RestTemplate restTemplate(){
      return new RestTemplate();
  }
}
```

