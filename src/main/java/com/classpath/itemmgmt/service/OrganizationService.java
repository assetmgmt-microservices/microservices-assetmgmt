package com.classpath.itemmgmt.service;

import com.classpath.itemmgmt.model.Organization;

public interface OrganizationService {

    Organization getOrgById(int id);
}