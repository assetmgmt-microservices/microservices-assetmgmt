package com.classpath.itemmgmt.controller;

import com.classpath.itemmgmt.model.Item;
import com.classpath.itemmgmt.model.Organization;
import com.classpath.itemmgmt.service.ItemService;
import com.classpath.itemmgmt.service.OrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/v1/assets")
@RefreshScope
public class AssetController {

    private static final Logger logger = LoggerFactory.getLogger(AssetController.class);


    private ItemService assetService;

    @Value("${app.env}")
    private String env;

    @Value("${custom.message}")
    private String customMessage;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private OrganizationService organizationService;

    public AssetController(ItemService assetService){
        this.assetService = assetService;
    }

    @PostMapping

    @ResponseStatus(HttpStatus.CREATED)
    public Item saveItem(@RequestBody Item item){
        logger.debug("Came inside the item {}", item);
        return item;
    }

    @GetMapping
    public List<Item> fetchAll(){
        logger.debug("Fetching all the items ");
        return this.assetService.listAll();
    }

    @GetMapping(value = "/{itemId}")
    public Item fetchItemById( @PathVariable ("itemId") long itemId){
        return this.assetService.findById(itemId);
    }

    @PutMapping(value = "/{itemId}")
    public Item fetchItemById( @PathVariable ("itemId") long itemId, @RequestBody Item item){
        return this.assetService.update(itemId, item);
    }

    @DeleteMapping(value = "/{itemId}")
    public void deleteItemById( @PathVariable ("itemId") long itemId){
        this.assetService.deleteItemById(itemId);
    }


    @GetMapping("/beans")
    public List<String> getBeans(){
        return Arrays.asList(this.applicationContext.getBeanDefinitionNames());
    }

    @GetMapping("/env")
    public String getCurrentenv(){
        return "{\"env\": \""+this.env+"\"}";
    }

    @GetMapping("/message")
    public String getCustomMessage(){
        return "{\"message\": \""+this.customMessage+"\"}";
    }

    @GetMapping("/org/{id}")
    public Organization getOrganizationById(@PathVariable("id") int id){
        logger.debug("Fetching organization details by id {} ", id);
        return this.organizationService.getOrgById(id);
    }

    //@ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleRuntTimeException(Exception exception){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Exception thrown from the asset controller error handler");
    }
}